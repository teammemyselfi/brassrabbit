<?php

namespace App\Libraries;

class MessageTemplate
{
    public function render($templates, $contacts)
    {
        $contactTemplates = [];
        $mainTemplate = [];

        if (!is_array($templates))
        {
            $templates = array($templates);
        }

        foreach ($contacts as $contact)
        {
            $contents = $this->selectTemplate($contact, $templates);
            $contents = '<?php echo "' . $contents . '"; ?>';
            $contents = preg_replace('/{{ (\w+) \}}/', '{$contact["$1"]}', $contents);

            // save template to temp file
            $filename = public_path("storage") . "\messateTemplateTemp" .  time() . ".php";
            file_put_contents($filename, $contents);

            $contactTemplates[] = $this->render_template($filename, $contact);

            // delete template temp file
            if (\File::exists($filename))
            {
                \File::delete($filename);
            }
        }

        foreach($contactTemplates as $t)
        {
            $subject = [];
            $message = [];

            preg_match('/\[subject\](.*?)\[subject\]/', $t, $subject);
            preg_match('/\[message\](.*?)\[message]/', $t, $message);

            $mainTemplate[] = ['subject' => $subject[1], 'message' => $message[1]];
        }

        return $mainTemplate;
    }

    private function selectTemplate($contact, $templates)
    {
        $t = '';
        $keywords = [];

        foreach($templates as $template)
        {
            $t = $template;

            preg_match_all('/{{ (.*?) }}/', $template, $keywords);

            if ($this->isContactColumnEmpty($contact, $keywords))
            {
                continue;
            }

            $t = $template;

            break;
        }

        return $t;
    }

    private function isContactColumnEmpty($contact, $keywords)
    {
        foreach($keywords[1] as $key => $value)
        {
            if (!array_key_exists($value, $contact))
            {
                continue;
            }

            if ($contact[$value] == '')
            {
                return true;
            }
        }
    }

    private function render_template()
    {
        ob_start ();

        $contact = func_get_args()[1];

        include func_get_args()[0];
        return ob_get_clean();
    }
}

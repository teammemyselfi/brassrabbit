<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use URL;

class HomeController extends Controller
{
    protected $template;

    public function __construct(\App\Libraries\MessageTemplate $messageTemplate)
    {
        $this->template = $messageTemplate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('home');
    }

    public function submit(Request $request)
    {
        $userTemplates = $request->input('templates');

        $templates = array();

        foreach($userTemplates as $key => $value)
        {
            $templates[] = "[subject]{$value['subject']}[subject][message]{$value['message']}[message]";
        }

        $contacts = $this->lookupDictionary();
        $renderedTemplate = $this->template->render($templates, $contacts);

        return response()->json([ "Success" => true, "Message" => "success", "Response" => $renderedTemplate ]);
    }

    public function uploadContacts(Request $request)
    {
        if (!$request->hasFile("file"))
        {
            return response()->json("Error uploading contact.", 200);
        }

        $file = $request->file("file");
        $fileName = $file->getClientOriginalName();
        $file->move(public_path("storage"), $fileName);

        return response()->json("Upload success.", 200);
    }

    private function lookupDictionary()
    {
        $csv = @fopen(public_path("storage") . "/Contacts.csv", 'r');

        $keys = fgetcsv($csv);

        $dictionary = array();

        while ($values = fgetcsv($csv))
        {
            $dictionary[] = array_combine($keys, $values);
        }

        return $dictionary;
    }
}

$(function()
{
    $("#addTemplate").click(function (e)
    {
        $("<div />", { "class": "newTemplate", html: cloneFormTemplate() }).hide().appendTo(".formTemplateContainer").slideDown("slow");
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var templates = [] = createTemplates();

        $.ajax(
        {
            url: 'home/submit',
            type: "POST",
            dataType: "json",
            data: {templates: templates},
            success: function(data)
            {
                submitSuccess(data);
            },
            error: function(data)
            {
                console.log(data);
            }
        });
    });

    function submitSuccess(data)
    {
        $(".alert").removeClass().addClass("alert").html('');

        if (data.Success == false)
        {
            $(".alert").removeClass().addClass("alert alert-danger");
            $(".alert").html(data.Message);

            return;
        }

        var arrayResponse = data.Response;

        if (arrayResponse < 1)
        {
            return;
        }

        var output = '<div class="row border">';

        $.each(arrayResponse, function(index, value)
        {
            $(".output").removeClass("d-none");
            output += '<div class="col-sm-3 border">';
            output += value.subject;
            output += '</div>';

            output += '<div class="col-sm-9 border">';
            output +=  value.message;
            output += '</div>';
        });

        output += '</div>';

        $(".outputContainer").html(output);
    }

    function cloneFormTemplate()
    {
        return $(".formTemplate").clone().html();
    }

    function createTemplates()
    {
        var templates = [], subject = [], message = [];

        $('input[name="subject[]"]').each(function()
        {
            subject.push(this.value);
        });

        $('textarea[name="message[]"]').each(function()
        {
            message.push(this.value);
        });

        for (i=0; i< subject.length; i++)
        {
            templates.push({
                "subject": subject[i],
                "message": message[i]
            });
        }

        return templates;
    }
});

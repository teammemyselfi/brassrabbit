@extends('layouts.master')

@section('title', 'Laravel')

@section('styles')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" integrity="sha512-3g+prZHHfmnvE1HBLwUnVuunaPOob7dpksI7/v6UnF/rnKGwHf/GdEq9K7iEN7qTtW+S0iivTcGpeTBqqB04wA==" crossorigin="anonymous" />

    <style>
        .title {
            font-size: 84px;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            margin: 0;
            text-align: center;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="title col-sm-12">
            Brass Rabbit
        </div>
    </div>

    <div class="row">
        <div class="col-sm">
            <div class="alert"></div>
        </div>
    </div>

    <div class="row m-b-md">
        <div class="col-sm">
            <div class="form-group row">
                <label for="inputSubject" class="col-sm-2 col-form-label">Upload CSV</label>
                <div class="col-sm-10">
                    <form action="home/uploadContacts" class="dropzone" id="myDropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                </div>
            </div>
            <form class="templateForm">
                <div class="formTemplateContainer">
                    <div class="formTemplate">
                        <div class="form-group row">
                            <label for="inputSubject" class="col-sm-2 col-form-label">Subject</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control subject" name="subject[]" placeholder="Subject" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="textMessage" class="col-sm-2 col-form-label">Message</label>
                            <div class="col-sm-10">
                                <textarea class="form-control message" name="message[]" rows="3" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <hr />
                    </div>
                </div>
                <button type="button" class="btn btn-success" id="addTemplate">Add New Template</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" id="submit">Submit</button>
            </form>
        </div>
    </div>

    <div class="output d-none mt-5">
        <div class="row border">
            <div class="col-sm-3 bg-info text-light p-2">
                <span>Subject</span>
            </div>
            <div class="col-sm-9 bg-info text-light p-2">
                <span>Message</span>
            </div>
        </div>
        <div class="outputContainer">
        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js" integrity="sha512-9WciDs0XP20sojTJ9E7mChDXy6pcO0qHpwbEJID1YVavz2H6QBz5eLoDD8lseZOb2yGT8xDNIV7HIe1ZbuiDWg==" crossorigin="anonymous"></script>
    <script src="{{ URL::to('/js/scripts.js') }}"></script>

    <script>
        Dropzone.options.myDropzone =
        {
            maxFiles: 1,
            acceptedFiles:".csv",
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            init: function()
            {
            }
        };
    </script>
@endsection
